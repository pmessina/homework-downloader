import pickle

import tkinter as tk
from tkinter import ttk

from website import Website
from file_work import *

LARGE_FONT = ("Verdana", 12)
NORMAL_FONT = ("Verdana", 10)
SMALL_FONT =  ("Verdana", 8)


def popupmsg(msg):
    """
    Creates a popup message. In general I use it to let users know that 
    a feature is not supported. In the future this will let users know of an
    error (such as incorrect username or password)
    """
    popup = tk.Tk()
    popup.wm_title("!")
    label = ttk.Label(popup, text = msg, font = NORMAL_FONT)
    label.pack(side="top", fill="x", pady=10)
    ttk.Button(popup, text="Okay", command = popup.destroy).pack()
    popup.mainloop()



class AppWindow(tk.Tk):
  """
  This class will make the tkinter window and cycle through the other windows
  StartPage and Settings
  """

  def __init__(self, *args, **kwargs):
    """
    Intializes the window, menu, and allows for movement through pages.
    """
                
    tk.Tk.__init__(self, *args, **kwargs)

    tk.Tk.wm_title(self, "Alpha: Homework Download and Upload Program")

    container = tk.Frame(self)
    container.pack(side="top",fill="both", expand=True)
    container.grid_rowconfigure(0, weight=1)
    container.grid_columnconfigure(0, weight=1)

    #Inserts a menubar, and will contain settings and help sections
    menubar = tk.Menu(container)
    filemenu = tk.Menu(menubar, tearoff=0)
    filemenu.add_command(label="Save settings", command = lambda: popupmsg("This is not yet supported"))
    filemenu.add_separator()
    filemenu.add_command(label="Exit",command = quit)
    menubar.add_cascade(label = "File", menu = filemenu)

    helpmenu = tk.Menu(menubar, tearoff = 0)
    helpmenu.add_command(label="How to", command = lambda: popupmsg("This is not yet supported"))
    helpmenu.add_command(label="Advanced Help", command = lambda: popupmsg("This is not yet supp)orted"))

    menubar.add_cascade(label = "Help", menu = helpmenu)
    tk.Tk.config(self, menu = menubar)

    self.frames = {}
        
    for F in (StartPage, Settings, Help):
        frame = F(container,self)
        self.frames[F] = frame
        frame.grid(row=0, column = 0, sticky="nsew")

        self.show_frame(StartPage) # EDIT need to fix this for the general case.


  def show_frame(self, cont):
    frame = self.frames[cont]
    frame.tkraise()


class StartPage(tk.Frame):
  """
  This is the start up page. From here the user can upload and download the 
  homework. 
  """
  def __init__(self, parent, controller):
    """
    Adds the form, gets saved data and inputs it into the entries.
    """

    tk.Frame.__init__(self, parent)

    ttk.Label(self, text="Username", font = NORMAL_FONT).grid(row=1,
            sticky='W', padx = 5)
    ttk.Label(self, text="Password", font = NORMAL_FONT).grid(row=2,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Homework Number", font = NORMAL_FONT).grid(row=3,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Class", font = NORMAL_FONT).grid(row=4,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Section Number", font = NORMAL_FONT).grid(row=5,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Starting ID", font = NORMAL_FONT).grid(row=6,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Ending ID", font = NORMAL_FONT).grid(row=7,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Initals", font = NORMAL_FONT).grid(row=8,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Destination Path", font = NORMAL_FONT).grid(row=9,
            sticky='W', padx = 5)
    ttk.Label(self, text = "Download Path", font = NORMAL_FONT).grid(row=10,
            sticky='W', padx = 5)
    

    ttk.Label(self, text = "Path to where files are stored.", font = NORMAL_FONT).grid(row=9,
            column = 2, sticky='W', padx = 5)
    ttk.Label(self, text = "Path to your download folder.", font = NORMAL_FONT).grid(row=10,
            column = 2, sticky='W', padx = 5)


    self.usernameEntry = ttk.Entry(self)
    self.passwordEntry = ttk.Entry(self, show="*")
    self.homeworkEntry = ttk.Entry(self)
    self.classnameEntry = ttk.Entry(self)
    self.sectionEntry = ttk.Entry(self)
    self.startingEntry = ttk.Entry(self)
    self.endingEntry = ttk.Entry(self)
    self.initalsEntry = ttk.Entry(self)
    self.destination_path = ttk.Entry(self)
    self.download_path = ttk.Entry(self)

    self.usernameEntry.grid(row = 1, column = 1, padx = 5, ipadx =6)
    self.passwordEntry.grid(row = 2, column = 1, padx = 5, ipadx =6)
    self.homeworkEntry.grid(row = 3, column = 1, padx = 5, ipadx =6)
    self.classnameEntry.grid(row = 4, column = 1, padx = 5, ipadx =6)
    self.sectionEntry.grid(row = 5, column = 1, padx = 5, ipadx =6)
    self.startingEntry.grid(row = 6, column = 1, padx = 5, ipadx =6)
    self.endingEntry.grid(row = 7, column = 1, padx = 5, ipadx =6)
    self.initalsEntry.grid(row = 8, column = 1, padx = 5, ipadx =6)
    self.destination_path.grid(row = 9, column = 1, padx = 5, ipadx =6)
    self.download_path.grid(row = 10, column = 1, padx = 5, ipadx =6)


        
    # trys to get previous data or intial values will be used
    self.get_saved_information()
  
    #inserts saved values
    self.classnameEntry.insert(10,self.class_name)
    self.sectionEntry.insert(10,self.section)
    self.startingEntry.insert(10,self.start)
    self.endingEntry.insert(10,self.ending)
    self.initalsEntry.insert(10, self.inital)
    self.destination_path.insert(10, self.destination)
    self.download_path.insert(10, self.download)
    # EDIT need to add destination path and download path to this

    # Download and Uplaod Buttons    
    ttk.Button(self, text='Download Homework', command
     = self.run_download).grid(row =11, column=1,  padx=4, pady=4)
    ttk.Button(self, text = 'Upload Homework', command
        = self.run_upload).grid(row = 12, column = 1, padx = 4, pady = 4)
 

  def get_entries(self):
    """
    Gets entries and saves them to file for future use.
    """
    self.username = self.usernameEntry.get()
    self.password = self.passwordEntry.get()
    self.hw_number = self.homeworkEntry.get()
    self.class_name = self.classnameEntry.get()
    self.section_number = self.sectionEntry.get()
    self.starting_id = self.startingEntry.get()
    self.ending_id = self.endingEntry.get()
    self.initals = self.initalsEntry.get()
    self.destination= self.destination_path.get()
    self.download = self.download_path.get()
        
    #The following code will save the data for future use
    #It uses the pickle module
    self.information_dictionary = {'className':self.class_name, 'section':self.section_number, 'start': self.starting_id, 'ending': self.ending_id, 'inital': self.initals, 'destinationPath': self.destination, 'downloadPath': self.download}

    pickle_out = open("dict.pickle","wb")
    pickle.dump(self.information_dictionary, pickle_out)
    pickle_out.close()

  def get_saved_information(self):
    """
    Load past data and saves to variables to insert
    """

    try:
      # If there is saved data, it will be gathered.
      pickle_in = open("dict.pickle","rb")
      information_dictionary = pickle.load(pickle_in)
      
      self.class_name = information_dictionary['className']
      self.section = information_dictionary['section']
      self.start = information_dictionary['start']
      self.ending = information_dictionary['ending']
      self.inital = information_dictionary['inital']
      self.destination = information_dictionary['destinationPath']
      self.download = information_dictionary['downloadPath']

      pickle_in.close()

    except:
      # if there is not saved data, this is what will be displayed in entrys.
      self.class_name = 'Example: MATH 1111'
      self.section = 'Section Number'
      self.start = 'Starting Student ID'
      self.ending = 'Ending Student ID'
      self.inital = 'Initals'
      self.destination = 'C:\\\\Users\\\\JohnDoe\\\\Desktop\\\\' 
      self.download = 'C:\\\\Users\\\\JohnDoe\\\\Downloads\\\\'

  def run_download(self):
    """
    Will download the homework, place it in a new file, and in the future
    should also open up in the users pdfannotator.
    """
    self.get_entries()
    self.get_saved_information()
    w = Website(self.username, self.password, self.section_number,
        self.class_name)
    w.selecting_students(self.hw_number, self.start, self.ending, self.inital)
    FileWork.transfer_zipped(self.inital, self.section_number, self.hw_number, self.destination, self.download)
    

  def run_upload(self):
    """
    Will run everything need to upload docuements. 
    """
    # EDIT currently not completed.
    
    self.get_entries()
    self.get_saved_information()
    # FileWork.zip_graded_assignments(self.inital, self.section_number, self.hw_number, self.destination)
    w = Website(self.username, self.password, self.section_number,
        self.class_name)
    w.get_to_upload_page(self.hw_number)


class Settings(tk.Frame):

  def __init__(self, parent, controller):
    tk.Frame.__init__(self, parent)
    #label = ttk.Label(self, text="Settings", font=LARGE_FONT)
    #label.pack(pady=10,padx=10)

    download_nav_button = ttk.Button(self, text="Download Homework", command = lambda: controller.show_frame(DownloadHW))
    download_nav_button.pack()
    upload_nav_button = ttk.Button(self, text="Upload Homework", command = lambda: controller.show_frame(UploadHW))
    upload_nav_button.pack()

class Help(tk.Frame):
  """
  This page will contain all the help functions.
  """
  def __init__(self, parent, controller):
    tk.Frame.__init__(self, parent)



