This software is one of the first programs I wrote, and actually used. I no
longer work as a teaching assistant, and therefore this program is no longer
maintained.

Known Issues
-----------------------------
* Currently users will have to set firefox to automatically download the
  the zip file once to the downloads page. This error does not occur with
  chrome, but chrome is harder to get selenium to work with. Download the [chrome
  drivers](https://sites.google.com/a/chromium.org/chromedriver/downloads)
* The code is neededing of some refractorig, it was one of the first things I wrote
  learning Python.
* Users using Windows have to manually add \\ to their filepaths, this should
  not be required of users.
* Having used this program for a few semesters, there will have to be some adjusting
  of the code. Some things I thought would not change, change based on how the proffessor
  and uploaded the homeworks.

How to use this program
--------------------------
* Developed in Python 3, but it should run in 2.7.
* Packages needed are the following:
      * time
      * pickle
      * shutil
      * os
      * zipfile
      * selenium
      * tkinter 
* In terminal you will need to go to where you have python and type 
  python school_app.py 
  where school_app is the filename is the name given to the python script that
  runs the program. If you have multiple versions of python installed, you will
  need to type python3 instead of python. 
* If you do not have a package you will need to use pip to download the 
  packages. 
* The program should be self explanatory for inputting the data. That said
  if your filepath uses \ you will need to type \\ as \ is an escape character.

Features of current program
---------------------------
* Logs onto CASA goes to the selected homework.
* Selects only the specified homeworks.
* Downloads the homeworks.
* Creates an organized filing system.
* Extracts the homework into the organized filing system.
* Logs onto CASA and goes to the upload the selected homework page.

