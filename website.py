from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time


class Website:
  """
  Logs into the website on initalization. Contains all the functions to
  navigating the website. 
  """

  def __init__(self, username_entered, password_entered, section_entered,
      class_entered):
    """
    Navigates to webpage, logs in, and navigates to assignments. 
    """
    self.browser = webdriver.Firefox()
    self.browser.get("https://www.casa.uh.edu/CourseWare2008/Root/Pages/Account/Login.aspx?ReturnUrl=%2fCourseWare2008%2fRoot%2fPages%2fLogin.aspx")
    time.sleep(5) #gives some time to makesure the page is loaded
    self.login(username_entered, password_entered)
    self.select_section(class_entered, section_entered)
    self.go_to_assignment()
  

  def login(self, username_entered, password_entered):
    """
    Logs into the website
    """
    username = self.browser.find_element_by_id("userNameInput")
    password = self.browser.find_element_by_id("passwordInput")
    username.send_keys(username_entered) 
    password.send_keys(password_entered) 
    login_attempt = self.browser.find_element_by_xpath("//*[@type='submit' and @id='loginButton']")
    login_attempt.submit()
    time.sleep(1)
       
  def select_section(self, class_entered, section_entered):
    """
    This function will select the correct section. 
    """
    find_section = self.browser.find_element_by_link_text(class_entered
        + ' ( ' + section_entered + ' )')
    type(find_section)
    find_section.click()
    time.sleep(3)

  def go_to_assignment(self):
    """
    Goes to assignment page
    """ 
    find_assignment = self.browser.find_element_by_link_text('Assignments')
    type(find_assignment)
    find_assignment.click()
    time.sleep(3)

  def selecting_students(self, hw_number, starting_id, ending_id, initals):
    """
    Navigates to student selection, zips files, and downloads homework.
    """
  
    #navigates to the Download Submitted Assignment page
    find_submitted = self.browser.find_element_by_link_text('Download Submitted Assignment')
    type(find_submitted)
    find_submitted.click()
    time.sleep(4)

    #selects the homework by number 
    select_hw = self.browser.find_element_by_id('whw' + str(hw_number) + '__homeworkNameSpan')
    type(select_hw)
    select_hw.click()
    time.sleep(3)
        
    #selects students 
    students_id = self.browser.find_elements_by_class_name('student_id_label')
    checkbox = self.browser.find_elements_by_name('checkBoxGroup1')
        
    for items,box in zip(students_id, checkbox):
        id_number = items.text 
        if int(id_number) >= int(starting_id) and int(id_number) <= int(ending_id):
          box.click()
       
    #insets initals to name file being zipped
    inital_input = self.browser.find_element_by_id("ctl00_ctl00_body_body_tbAdditionalName")
    inital_input.send_keys(initals)
        
    zip_selected_file = self.browser.find_element_by_css_selector("button[type='button'")
    zip_selected_file.click()
    time.sleep(60)

    self.browser.find_element_by_link_text('Show Zipped Files').click()
    time.sleep(3)
    download_link = self.browser.find_element_by_link_text('whw'+str(hw_number)+'-'+initals+'.zip')
    download_link.click()
    time.sleep(60) #waits for the file to download

  def get_to_upload_page(self, hw_number):
    """
    Will get to the page to upload hw, and upload assignment.
    """
    homework_numbers = self.browser.find_elements_by_partial_link_text('whw')
    click_here_links = self.browser.find_elements_by_link_text('Click Here')
   
    counter = -1 # -1 because lists start at 0, prevents an off by 1 error
    # We loop through and make sure to get the desired link
    # I am sure there is a nice work around for having a bunch of the same link
    # But I didn't see anything that worked here and so this is my solution.
    for homework_number in homework_numbers:
        counter += 2
        if homework_number.text == 'whw'+hw_number: 
            click_here_links[counter].click()
            break


