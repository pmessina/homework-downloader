class FileWork:
  """
  Makes files for grading assignments. Also extracts and zips needed files.

  I need to check compatability issues here.
  """
  def transfer_zipped(user_initials, section_number, hw_number, destinationPath, downloadPath):
	    time.sleep(60)

	    # fileName is the name of the zipfile
	    fileName = 'whw' + str(hw_number) + '-' + user_initials + '.zip'

	    # path_to_download_files is the  path to the dir. that the 
	    # the zipfile was downloaded to
	    path_to_downloaded_files =  downloadPath + fileName 

	    # path_to_grading_files will be the location where all the
	    # grading files will be stored.
	    path_to_grading_files = destinationPath + 'Grading_' + str(section_number) + '\\'

	    # path_to_hw_file is the path to the specific file for the homework
	    # That is for a given homework all files relating to that homework
	    # will be stored in this location
	    path_to_hw_file = path_to_grading_files + 'HW_' + str(hw_number)

	    graded = path_to_hw_file + '\\Graded'
	    not_graded = path_to_hw_file + '\\Not_Graded'

	    #checks to see if there is a dir. for grading
	    #if not one will be made
	    if not os.path.isdir(path_to_grading_files):
		    os.mkdir(path_to_grading_files)
	    if not os.path.isdir(path_to_hw_file):
		    os.mkdir(path_to_hw_file)
		    os.mkdir(graded)
		    os.mkdir(not_graded)

	    shutil.move(path_to_downloaded_files, path_to_hw_file)
	    
	    #moves to file and the unzips file
	    os.chdir(path_to_hw_file)
	    file_to_unzip = zipfile.ZipFile(fileName)
	    file_to_unzip.extractall(not_graded)
	    file_to_unzip.close()

  def zip_graded_assignments(user_initials, section_number, hw_number, destinationPath):
            """
            Currently not finished. I am still working on this. 

	    # fileName is the name of the zipfile
	    fileName = 'whw' + str(hw_number) + '-' + 'graded_' + user_initials + '.zip'

	    # path_to_grading_files will be the location where all the
	    # grading files will be stored.
	    path_to_grading_files = destinationPath + 'Grading_' + str(section_number) + '\\'

	    # path_to_hw_file is the path to the specific file for the homework
	    # That is for a given homework all files relating to that homework
	    # will be stored in this location
	    path_to_hw_file = path_to_grading_files + 'HW_' + str(hw_number)

	    graded = path_to_hw_file + '\\Graded'
	    
	    #moves to file and the unzips file
	    os.chdir(path_to_hw_file)
	    file_to_zip = zipfile.ZipFile(fileName, 'w')
	    file_to_zip.write(graded, compress_type=zipfile.ZIP_DEFLATED)
	    file_to_zip.close()
            """


